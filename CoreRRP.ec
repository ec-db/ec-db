require import AllCore SmtMap Distr FSet List FelTactic.
require (*  *) Environment MyPKS.
require (*--*) Mu_mem.
(*---*) import StdOrder.RealOrder.

(** Parameters **)
type pkey, skey, signature.

type nonce.
op nonces : { nonce list | forall n, count (pred1 n) nonces = 1 } as nonces_spec.

clone include MFinite with
  type t            <- nonce,
  op   Support.enum <- nonces
proof
  Support.enum_spec by exact/nonces_spec,
  *
rename "dunifin" as "dnonce".

(** Signature Security Definition **)
clone import MyPKS as PKS with
  type pkey      <- pkey,
  type skey      <- skey,
  type message   <- nonce * nonce,
  type signature <- signature.
import EF_CMA.

(** Protocol-specific environment **)
type name   = [ Card | Reader | Adv ].
type message =
  [ Challenge of nonce
  | Response  of nonce & signature ].

clone include Environment with
  type    name <- name,
  type message <- message.

(** Multi-instancing and interactions **)
type sid.

(* Protocol steps can output messages, to be sent over the network,
   and local outputs to be used locally (in ways controlled by the
   experiment/context) *)
type out = [ Reject | Accept ].

(* Protocol actions *)
type action = [ Timeout ].

(* Protocol transitions *)
type transition =
  [ TCard        of mhandle & mhandle option
  | TReaderInit  of sid
  | TReaderFinal of sid & mhandle & mhandle option].

module type Reader = {
  proc init(_ : real * pkey): unit

  (** Session procedures **)
  proc generateUN(): ctime * nonce
  proc verify(_ : ctime * nonce * nonce * signature) : out

  (** Multi-session transition interface **)
  proc send_challenge(_ : sid) : message option * out option
  proc recv_response(_ : sid * message) : message option * out option

  proc timedout() : unit
}.

module type Card = {
  proc init() : pkey

  proc generateNC() : nonce
  proc certify(_ : nonce) : nonce * signature

  (** Multi-session transition interface **)
  proc recv_challenge(_ : message) : message option * out option
}.

(* Oracles for Correctness and Security *)
module type Oracle = {
  proc init() : unit
  proc setup(_ : real) : pkey

  proc set_clock(_ : ctime) : unit
  proc get_clock() : ctime

  proc get_location(_ : name) : location option
  proc set_location(_ : name * location) : unit

  proc honest_action(_ : action) : unit
  proc honest_transition(_ : transition) : mhandle option * out option

  proc adversary_send(_ : message)                    : mhandle
  proc adversary_modify(_ : message -> message)       : mhandle
  proc adversary_read(_ : mhandle * (mhandle option)) : message option
}.

(****** Reader, Card, and Oracle Instantiations ******)
(* ***************************************************************************** *)
(* R : un ---> C
   C : nc,s -> R *)

(* Reader *)
module Reader (S : Scheme) : Reader = {
  var smap  : (sid, ctime * nonce) fmap
  var bound : real
  var cpk   : pkey

  proc init(b : real, pk : pkey) : unit={
    bound <- b;
     smap <- empty;
      cpk <- pk;
  }

  proc generateUN() : ctime * nonce = {
    var t, un;

     t <@ Env.get_clock();
    un <$ dnonce;
    return (t, un);
  }

  proc verify(st : ctime, sn : nonce, nc : nonce, sig : signature) : out = {
    var t, r, ev;

    t <@ Env.get_clock();
    if (dt t st <= bound) {
      ev <@ S.verify(cpk, (sn, nc), sig);
       r <- if ev then Accept else Reject;
    } else {
      r <- Reject;
    }
    return r;
  }

  proc send_challenge(sid : sid) : message option * out option = {
    var t, un;
    var r <- None;

    if (sid \notin smap) {
         (t, un) <@ generateUN();
      smap.[sid] <- (t, un);
               r <- Some (Challenge un);
    }
    return (r, None);
  }

  proc recv_response(sid : sid, m : message) : message option * out option = {
    var st, sn, ro;
    var r <- None;

    match smap.[sid] with
    | None   => { }
    | Some s => {
      (st, sn) <- s;
      match m with
      | Challenge _      => { }
      | Response  nc sig => {
          ro <@ verify(st, sn, nc, sig);
           r <- Some ro;
        }
      end;
      smap <- rem smap sid;
      }
    end;
    return (None, r);
  }

  proc timedout(): unit = {
    var t;

       t <@ Env.get_clock();
    smap <- filter (fun x (y : _ * _), dt t y.`1 <= bound) smap;
  }
}.

(* Card *)
module Card (S : Scheme) : Card = {
  var sk : skey

  proc init() : pkey = {
    var r;
    (r, sk) <@ S.keygen();
    return r;
  }

  proc generateNC() : nonce = {
    var nc;

    nc <$ dnonce;
    return nc;
  }

  proc certify(un : nonce) : nonce * signature = {
    var nc, sig;

    nc  <@ generateNC();
    sig <@ S.sign(sk, (un, nc));
    return (nc, sig);
  }

  proc recv_challenge(m : message) : message option * out option = {
    var ro;
    var r <- None;

    match m with
    | Challenge nr  => {
        ro <@ certify(nr);
         r <- Some (Response ro.`1 ro.`2);
      }
    | Response  _ _ => { }
    end;
    return (r, None);
  }
}.

(* Oracle  *)
module Oracle (C : Card) (R : Reader) : Oracle ={
  proc init = Env.init

  proc setup(bound : real) : pkey = {
    var pk;

    pk <@ C.init();
          R.init(bound, pk);
    return pk;
  }

  proc get_clock = Env.get_clock
  proc set_clock = Env.set_clock

  proc get_location = Env.get_location
  proc set_location = Env.set_location

  proc honest_action(a : action) = {
    match a with
    | Timeout => {
        R.timedout();
      }
    end;
  }

  proc sendo(p, m : message option) = {
    var h;
    var ho <- None;

    match m with
    | None   => { }
    | Some m => {
        h <@ Env.send(p, m);
        ho <- Some h;
      }
    end;
    return ho;
  }

  proc honest_transition(tr : transition) = {
    var p;
    var m  <- None;
    var r  <- None;
    var ho <- None;

    match tr with
    | TCard mh rh => {
        p <- Card;
        m <@ Env.recv(p, mh, rh);
        match m with
        | None    => { }
        | Some m' => {
            (m, r) <@ C.recv_challenge(m');
          }
        end;
      }

    | TReaderInit sid => {
             p <- Reader;
        (m, r) <@ R.send_challenge(sid);
      }

    | TReaderFinal sid mh rh => {
        p <- Reader;
        m <@ Env.recv(p, mh, rh);
        match m with
        | None    => { }
        | Some m' => {
            (m, r) <@ R.recv_response(sid, m');
          }
        end;
      }
    end;
    ho <@ sendo(p, m);

    return (ho, r);
  }

  proc adversary_modify(f : message -> message) = {
    var h;

    h <@ Env.modify(Adv, f);
    return h;
  }

  proc adversary_read(h : mhandle, rh : mhandle option) : message option = {
    var m <- None;

    m <@ Env.recv(Adv, h, rh);
    return m;
  }

  proc adversary_send(m : message) : mhandle = {
    var h;

    h <@ Env.send(Adv, m);
    return h;
  }
}.


(****** Security properties and proofs ******)
(* ***************************************************************************** *)

theory Basic.
(* Basic Security: no modify during challenge session *)
module type Adv_BSec (O : Oracle) ={
  proc start(): real * sid { O.set_location }
  proc phase1(pk : pkey): unit
     { O.get_clock, O.set_clock, O.get_location, O.set_location,
       O.honest_transition, O.honest_action,
       O.adversary_send, O.adversary_modify, O.adversary_read }
  proc main(_ : mhandle): ctime * nonce * signature
     { O.get_clock, O.set_clock, O.get_location,
       O.honest_action,
       O.adversary_send, O.adversary_modify, O.adversary_read }
}.

(* Basic Security Experiment *)
module Exp_Adv_BSec (A : Adv_BSec) (C : Card) (R : Reader) = {
  proc main() : bool = {
    var tc, ncc, bound, sidc, sigc, mho, mh, pk;
    var o <- None;

    Oracle(C, R).init();

    (bound, sidc) <@ A(Oracle(C, R)).start();
               pk <@ Oracle(C, R).setup(bound);

    A(Oracle(C, R)).phase1(pk);

    if (bound < 2%r * dl (oget Env.lmap.[Reader]) (oget Env.lmap.[Card])) {
      (mho, o) <@ Oracle(C, R).honest_transition(TReaderInit sidc);

      if (mho <> None) {
        (tc, ncc, sigc) <@ A(Oracle(C, R)).main(oget mho);

        Env.set_clock(tc);
        mh <@ Oracle(C, R).adversary_send(Response ncc sigc);

        Env.set_clock(dl (oget Env.lmap.[Adv]) (oget Env.lmap.[Reader]));
        (mho, o) <@ Oracle(C, R).honest_transition(TReaderFinal sidc mh None);
      }
    }
    return o = Some Accept;
  }
}.

(** The Reduction : cast reader, card and experiment as
    an adversary against the signing scheme, constrained by the environment  **)
module Reader0 (S : PScheme) : Reader = {
  proc init(b : real, pk : pkey) : unit = {
    Reader.bound <- b;
     Reader.smap <- empty;
      Reader.cpk <- pk;
  }

  proc generateUN() : ctime * nonce = {
    var t, un;

     t <@ Env.get_clock();
    un <$ dnonce;
    return (t, un);
  }

  proc verify(st : ctime, sn : nonce, nc : nonce, sig : signature) : out = {
    var t, r, ev;

    t <@ Env.get_clock();
    if (dt t st <= Reader.bound) {
      ev <@ S.verify((sn, nc), sig);
       r <- if ev then Accept else Reject;
    } else {
      r <- Reject;
    }
    return r;
  }

  proc send_challenge(sid : sid) : message option * out option = {
    var t, un;
    var r <- None;

    if (sid \notin Reader.smap) {
                (t, un) <@ generateUN();
      Reader.smap.[sid] <- (t, un);
                      r <- Some (Challenge un);
    }
    return (r, None);
  }

  proc recv_response(sid : sid, m : message) : message option * out option = {
    var st, sn, ro;
    var r <- None;

    match Reader.smap.[sid] with
    | None          => { }
    | Some s => {
      (st, sn) <- s;
      match m with
      | Challenge _      => { }
      | Response  nc sig => {
          ro <@ verify(st, sn, nc, sig);
           r <- Some ro;
        }
      end;
      Reader.smap <- rem Reader.smap sid;
      }
    end;
    return (None, r);
  }

  proc timedout(): unit = {
    var t;

              t <@ Env.get_clock();
    Reader.smap <- filter (fun x (y : _ * _), dt t y.`1 <= Reader.bound) Reader.smap;
  }
}.

module Card0 (S : PScheme) : Card = {
  proc init() : pkey = {
    return witness;
  }

  proc generateNC() : nonce = {
    var nc;

    nc <$ dnonce;
    return nc;
  }

  proc certify(un : nonce) : nonce * signature = {
    var nc, sig;

    nc  <@ generateNC();
    sig <@ S.sign(un, nc);
    return (nc, sig);
  }

  proc recv_challenge(m : message) : message option * out option = {
    var ro;
    var r <- None;

    match m with
    | Challenge nr  => {
        ro <@ certify(nr);
         r <- Some (Response ro.`1 ro.`2);
      }
    | Response  _ _ => { }
    end;
    return (r, None);
  }
}.

module Reduction (A : Adv_BSec) (S : PScheme) = {
  var pk   : pkey

  module O : Oracle = {
    proc init() = {
      Env.init();
    }

    proc setup(bound) : pkey = {
      Reader0(S).init(bound, pk);
      return pk;
    }

    proc get_clock = Env.get_clock
    proc set_clock = Env.set_clock

    proc get_location = Env.get_location
    proc set_location = Env.set_location

    proc honest_action(a : action) = {
      match a with
      | Timeout => {
          Reader0(S).timedout();
        }
      end;
    }

    proc sendo(p, m : message option) = {
      var h;
      var ho <- None;

      match m with
      | None   => { }
      | Some m => {
          h <@ Env.send(p, m);
          ho <- Some h;
        }
      end;
      return ho;
    }

    proc honest_transition(tr : transition) = {
      var p;
      var m  <- None;
      var r  <- None;
      var ho <- None;

      match tr with
      | TCard mh rh => {
          p <- Card;
          m <@ Env.recv(p, mh, rh);
          match m with
          | None    => { }
          | Some m' => {
              (m, r) <@ Card0(S).recv_challenge(m');
            }
          end;
        }
      | TReaderInit sid => {
               p <- Reader;
          (m, r) <@ Reader0(S).send_challenge(sid);
        }
      | TReaderFinal sid mh rh => {
          p <- Reader;
          m <@ Env.recv(p, mh, rh);
          match m with
          | None    => { }
          | Some m' => {
              (m, r) <@ Reader0(S).recv_response(sid, m');
            }
          end;
        }
      end;
      ho <@ sendo(p, m);

      return (ho, r);
    }

    proc adversary_read(h : mhandle, rh : mhandle option) : message option = {
      var m;

      m <@ Env.recv(Adv, h, rh);
      return m;
    }

    proc adversary_modify(f : message -> message) : mhandle = {
      var h;

      h <@ Env.modify(Adv, f);
      return h;
    }

    proc adversary_send(m : message) : mhandle = {
      var h;

      h <@ Env.send(Adv, m);
      return h;
    }
  }

  var unc  : nonce
  var ncc  : nonce
  var sigc : signature

  proc forge(pk0) = {
    var tc, bound, sidc, h;

    pk <- pk0;
          O.init();

    (bound, sidc) <@ A(O).start();

    O.setup(bound);

    A(O).phase1(pk);

    unc <$ dnonce;
      h <@ Env.send(Reader, Challenge unc);

    (tc, ncc, sigc) <@ A(O).main(h);

    (** If the adversary makes the BSec experiment return true, then its output is a valid forgery unless (unc, ncc) \in qs **)

    return ((unc, ncc), sigc);
  }
}.

section Security_Proof.
declare module SS <: Scheme  { -Env, -Card, -Reader, -Oracle, -Wrap, -Reduction }.

declare axiom SS_kll: islossless SS.keygen.
declare axiom SS_sll: islossless SS.sign.
declare axiom SS_vll: islossless SS.verify.

declare module AS <: Adv_BSec { -Env, -Card, -Reader, -Oracle, -Wrap, -Reduction, -SS }.

axiom AS_mll (O <: Oracle { -AS }):
     islossless O.get_clock
  => islossless O.set_clock
  => islossless O.get_location
  => islossless O.honest_action
  => islossless O.adversary_send
  => islossless O.adversary_modify
  => islossless O.adversary_read
  => islossless AS(O).main.

local equiv Direct :
  Exp_Adv_BSec(AS, Card(SS), Reader(SS)).main ~ EF_CMA(Wrap(SS), Reduction(AS)).main:
    ={glob AS, glob SS}
    ==> (!(Reduction.unc, Reduction.ncc) \in Wrap.qs){2} => (res{1} => res{2}).
proof.
proc.
inline Reduction(AS,Wrap(SS)).forge.
seq  5  7: (   ={glob AS, glob Env, glob SS, glob Reader}
            /\ ={sk}(Card,Wrap)
            /\ Reader.cpk{1} = Reduction.pk{2}
            /\ Reader.cpk{1} = Wrap.pk{2}
            /\ Reader.cpk{1} = pk{2}
            /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
            /\ o{1} = None).
+ inline *; auto.
  call (:    ={glob Env, glob SS, glob Reader}
          /\ ={sk}(Card,Wrap)
          /\ Reader.cpk{1} = Reduction.pk{2}
          /\ Reader.cpk{1} = Wrap.pk{2}
          /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}).
  + by conseq (: ={res}) _ wfe_get_clock=> //; sim.
  + by conseq (: ={Env.clock}) _ wfe_set_clock=> //; sim.
  + by conseq (: ={res}) _ wfe_get_location=> //; sim.
  + by conseq (: ={Env.lmap}) _ wfe_set_location=> //; sim.
  + conseq (:    ={glob Env, glob SS, glob Reader, res}
              /\ ={sk}(Card, Wrap)
              /\ Reader.cpk{1} = Reduction.pk{2}
              /\ Reader.cpk{1} = Wrap.pk{2})
           _
           (: well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              ==> well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> |>.
    + proc. inline Reduction(AS, Wrap(SS)).O.sendo.
      seq  7: (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock); last first.
      + exists * m0; elim *=> - [|m].
        + by match None 1; auto.
        match Some 1; first by auto=> /#.
        by auto; call wfe_send; auto.
      exists * tr; elim * => - [mh rh|sid|sid0 mh rh].
      + match TCard 4; first by auto=> /#.
        seq  5: (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock).
        + by call wfe_recv; auto.
        exists * m; elim * => - [|m].
        + by match None 1; auto.
        match Some 1; first by auto=> /#.
        inline *; case @[ambient]: m=> [nc|nr sig].
        + match Challenge 3.
          + by auto=> |> _ _; exists nc.
          by auto; call (: true); auto.
        match Response 3.
        + by auto=> |> _ _; exists nr sig.
        by auto.
      + match TReaderInit 4; first by auto=> /#.
        by inline *; sp; if; auto.
      match TReaderFinal 4; first by auto=> /#.
      seq  5: (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock).
      + by call wfe_recv; auto.
      exists * m; elim * => - [|m].
      + by match None 1; auto.
      match Some 1; first by auto=> /#.
      inline *. exists * Reader.smap.[sid]; elim * => - [|stc].
      + match None 4; first by auto=> /#.
        by auto.
      match Some 4; first by auto=> /#.
      case @[ambient]: m=> [nc|nr sig].
      + match Challenge 6.
        + by auto=> |> _ _ _; exists nc.
        by auto.
      match Response 6.
      + by auto=> |> _ _ _; exists nr sig.
      by sp; if; auto; call (: true); auto.
    proc; inline Env.get_clock.
    sim; sp; match=> //= [mh1 rh1 mh2 rh2|sid1 sid2|sid1 mh1 rh1 sid2 mh2 rh2].
    + seq  2  2: (   ={glob Env, glob SS, glob Reader, m, p, r, ho}
                  /\ ={sk}(Card,Wrap)
                  /\ Reader.cpk{1} = Reduction.pk{2}
                  /\ Reader.cpk{1} = Wrap.pk{2}
                  /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}).
      + call (: ={glob Env, arg} ==> ={res}); first by sim.
        by auto.
      match=> //= m1 m2.
      seq  1  1: (   ={glob Env, glob SS, glob Reader, m, p, r, ho}
                  /\ ={sk}(Card,Wrap)
                  /\ Reader.cpk{1} = Reduction.pk{2}
                  /\ Reader.cpk{1} = Wrap.pk{2}
                  /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2})=> //=.
      call (:    ={glob Env, glob SS, arg}
              /\ ={sk}(Card,Wrap)
              ==> ={glob SS, res}).
      + proc; sp; match=> //= n1 n2.
        by inline *; sim; call (: true); auto.
      by auto.
    + seq  2  2: (   ={glob Env, glob SS, glob Reader, m, p, r, ho}
                  /\ ={sk}(Card,Wrap)
                  /\ Reader.cpk{1} = Reduction.pk{2}
                  /\ Reader.cpk{1} = Wrap.pk{2}
                  /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2})=> //=.
      call (: ={glob Env, glob Reader, arg}
              ==> ={glob Reader, res}); first by sim.
      by auto.
    seq  2  2: (   ={glob Env, glob SS, glob Reader, m, p, r, ho}
                /\ ={sk}(Card,Wrap)
                /\ Reader.cpk{1} = Reduction.pk{2}
                /\ Reader.cpk{1} = Wrap.pk{2}
                /\ sid1 = sid2
                /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2})=> //=.
    + call (: ={glob Env, glob Reader, arg}
              ==> ={glob Reader, res}); first by sim.
      by auto.
    match=> //= m1 m2.
    call (:    ={glob SS, glob Reader, arg}
            /\ Reader.cpk{1} = Wrap.pk{2}
            ==> ={glob SS, glob Reader, res}).
    + proc.
      sp; match=> //= st1 st2; sp; match=> //= [nc1 nc2|nr1 sig1 nr2 sig2].
      + by auto.
      wp; inline *; sp; if=> /> => [+ + ->||] //; auto.
      by wp; call (: true); auto=> /> + + ->.
    by auto.
  + by conseq (: ={glob Reader})=> //; sim.
  + conseq (: ={glob Env, res}) _ (: well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock ==> well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> |>.
    + by proc; call wfe_send.
    by sim.
  + conseq (: ={glob Env, res}) _ (: well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock ==> well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> |>.
    + by proc; call wfe_modify.
    by sim.
  + by conseq (: ={res})=> //; sim.
  inline *. swap{1} [10..11] -9.
  wp; call (: ={glob Env} /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}).
  + by conseq (: ={glob Env}) _ wfe_set_location=> //; sim.
  wp; call (: true); auto=> //=.
  auto=> |>; do !split=> //.
  + by move=> mh; apply/WFmh_None=> [|/#]; exact/emptyE.
  by move=> mh; apply/WFrh_None=> [|/#]; exact/emptyE.
if{1}; last first.
+ conseq (: _ ==> true) (: o = None ==> o = None)=> //=.
  conseq _ _ (: _ ==> true).
  islossless.
  + exact/SS_vll.
  apply/(AS_mll (<: Reduction(AS, Wrap(SS)).O) _); islossless.
  + by inline *; auto=> /#.
  + by auto=> /#.
  by auto=> /#.
case: (Reader.smap.[sidc]{1} = None); last first.
+ inline {1} 1. inline {1} 6.
  match TReaderInit {1} 5; first by auto=> /#.
  inline {1} 7.
  rcondf {1}  9; first by auto; rewrite /get_as_TReaderInit.
  match None {1} 13; first by auto.
  rcondf {1} 15; first by auto.
  auto=> //=.
  conseq _ _ (: _ ==> true).
  islossless.
  + exact/SS_vll.
  apply/(AS_mll (<: Reduction(AS, Wrap(SS)).O) _); islossless.
  + by inline *; auto=> /#.
  + by auto=> /#.
  by auto=> /#.
rcondt{1} 2.
+ auto; inline *.
  match TReaderInit  5; first by auto=> /#.
  rcondt  9; first by auto=> /#.
  match Some 18; first by auto=> /#.
  by auto.
exists* Env.clock{1}; elim* => t0.
seq  1  2: (   ={glob AS, glob Env, glob SS, Reader.bound}
            /\ ={sk}(Card,Wrap)
            /\ Reader.cpk{1} = Reduction.pk{2}
            /\ Reader.cpk{1} = Wrap.pk{2}
            /\ Reader.cpk{1} = pk{2}
            /\ eq_except (pred1 sidc{1}) Reader.smap{1} Reader.smap{2}
            /\ Env.nmap.[h]{2} = Some (t0, oget Env.lmap.[Reader]{1}, Challenge (Reduction.unc{2}))
            /\ (Reader.smap.[sidc]{1} = Some (t0, Reduction.unc)){2}
            /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
            /\ o{1} = None
            /\ mho{1} = Some h{2}).
+ inline Oracle(Card(SS), Reader(SS)).honest_transition.
  match TReaderInit {1} 5; first by auto=> /#.
  inline Env.get_clock Reader(SS).send_challenge Reader(SS).generateUN Oracle(Card(SS), Reader(SS)).sendo.
  rcondt {1} 9; first by auto=> /#.
  match Some {1} 18; first by auto=> /#.
  inline *; auto=> |> &1 &2 wfe_inv _ smap_sidc nc _.
  rewrite /get_as_Some /get_as_TReaderInit //= !get_set_sameE eq_except_setl //=.
  do !split=> //; first 3 by (case: wfe_inv; smt()).
  + move=> mh; case: (mh = Env.mh{2})=> [->>|neq_mh].
    + apply/(WFmh_Some _ _ _ _ t0 (oget Env.lmap.[Reader]){2} (Challenge nc)).
      + by rewrite get_set_sameE.
      + by case: wfe_inv; smt().
      by case: wfe_inv; smt().
    move: wfe_inv=> @/well_formed_env [#] _ _ _ wfm_inv _.
    case: (wfm_inv mh)=> [t l m nmap_mh mh_bd t_bd|nmap_mh mh_bd].
    + by apply/(WFmh_Some _ _ _ _ t l m); rewrite ?get_set_neqE //#.
    by apply/WFmh_None; rewrite ?get_set_neqE //#.
  by move: wfe_inv=> @/well_formed_env.
seq  1  1: (   ={glob AS, glob Env, glob SS, Reader.bound, tc}
            /\ ncc{1} = Reduction.ncc{2}
            /\ sigc{1} = Reduction.sigc{2}
            /\ ={sk}(Card,Wrap)
            /\ Reader.cpk{1} = Reduction.pk{2}
            /\ Reader.cpk{1} = Wrap.pk{2}
            /\ Reader.cpk{1} = pk{2}
            /\ eq_except (pred1 sidc{1}) Reader.smap{1} Reader.smap{2}
            /\ Env.nmap.[h]{2} = Some (t0, oget Env.lmap.[Reader]{1}, Challenge (Reduction.unc{2}))
            /\ (Reader.smap.[sidc] <> None => Reader.smap.[sidc]{1} = Some (t0, Reduction.unc){2}){1}
            /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
            /\ o{1} = None).
+ exists * h{2}, Env.nmap.[h]{2}, sidc{1}, Reader.smap.[sidc]{1}; elim * => h + sidc +.
  case=> [|[] t_h l_h m_h]; first by move=> f; exfalso=> /#.
  case=> [|[] t_sidc nc_sidc]; first by exfalso=> /#.
  call (:    ={glob Env, glob SS, Reader.bound}
          /\ ={sk}(Card,Wrap)
          /\ Reader.cpk{1} = Reduction.pk{2}
          /\ Reader.cpk{1} = Wrap.pk{2}
          /\ eq_except (pred1 sidc) Reader.smap{1} Reader.smap{2}
          /\ (Env.nmap.[h] = Some (t0, oget Env.lmap.[Reader], Challenge Reduction.unc)){2}
          /\ (Reader.smap.[sidc] = None \/ Reader.smap.[sidc] = Some (t0, Reduction.unc{2})){1}
          /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}).
  + by conseq (: ={res})=> |>; sim.
  + by conseq (: ={glob Env}) _ wfe_set_clock=> //; sim.
  + by conseq (: ={res})=> |>; sim.
  + proc; match=> //; inline *; auto=> |> &1 &2 /eq_exceptP @/pred1 eqe_smap _ minitonic_smap _.
    split.
    + by rewrite eq_exceptP=> /= sid neq_sid; rewrite !filterE (eqe_smap _ neq_sid).
    by rewrite !filterE=> /=; case: (Reader.smap.[sidc]{1}) minitonic_smap=> //#.
  + conseq (: ={glob Env, res}) _
           (:    Env.nmap.[h] = Some (t0, oget Env.lmap.[Reader], Challenge Reduction.unc)
              /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              ==>    Env.nmap.[h] = Some (t0, oget Env.lmap.[Reader], Challenge Reduction.unc)
                  /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> |>; last by sim.
    conseq (:    Env.nmap.[h] = Some (t0, oget Env.lmap.[Reader], Challenge Reduction.unc)
              /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              ==> Env.nmap.[h] = Some (t0, oget Env.lmap.[Reader], Challenge Reduction.unc))
           (: well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock ==> well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> //.
    + by proc; call wfe_send.
    proc; inline *; auto=> |> &1 inv wfe_inv; rewrite !get_setE.
    move: wfe_inv=> @/well_formed_env [#] ge0_mh _ _ + _.
    by move=> /(_ h) []; rewrite inv /#.
  + conseq (: ={glob Env, res}) _
           (: well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              ==> well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> |>; last by sim.
    by proc; call wfe_modify.
  + by conseq (: ={glob Env, res})=> //; sim.
  by auto=> /#.
inline *.
match TReaderFinal {1} 19; first by auto=> /#.
match Some {1} 27.
+ auto=> @/get_as_TReaderFinal /> //=; rewrite get_setE=> //= _ _ _ _ _ _ _ _ _.
  by exists (Env.clock + if 0%r <= tc then tc else 0%r, oget Env.lmap.[Adv], Response Reduction.ncc Reduction.sigc){m}.
rcondt{1} 28.
+ auto=> @/get_as_Some @/get_as_TReaderFinal /> &hr; rewrite !get_set_sameE //=.
  by rewrite dl_sym; smt(ge0_dl).
match None {1} 29; first by auto=> /#.
match Some {1} 30; first by auto=> /#.
exists * Reader.smap.[sidc]{1}; elim * => - [|st].
+ match None {1} 34.
  + by auto=> @/get_as_TReaderFinal /> + <-.
  match None {1} 38; first by auto=> /#.
  by auto; call{2} SS_vll; auto.
match Some {1} 34.
+ by auto=> @/get_as_TReaderFinal /> + <- - _ _ _ _ _ _ _ _ _; exists st.
match Response {1} 36.
+ auto=> @/get_as_Some @/get_as_TReaderFinal |> &hr _ _ _ _ _.
  by exists Reduction.ncc{m} Reduction.sigc{m}; rewrite get_set_sameE.
sp; if{1}=> //=.
+ auto; call (: true); auto=> |> &1 &2.
  move=> @/get_as_TReaderFinal @/get_as_Some @/get_as_Response |>.
  rewrite get_set_sameE=> |> + /eq_sym Hsmap.
  by rewrite Hsmap=> /= <<- _ _ [] <<- ->> //= _ _ [].
by auto; call{2} SS_vll; auto.
qed.

lemma asecurity &m:
  Pr[Exp_Adv_BSec(AS, Card(SS), Reader(SS)).main() @ &m: res]
  <=   Pr[EF_CMA(Wrap(SS), Reduction(AS)).main() @ &m: res]
     + Pr[EF_CMA(Wrap(SS), Reduction(AS)).main() @ &m: (Reduction.unc, Reduction.ncc) \in Wrap.qs].
proof. by byequiv Direct. qed.

lemma pr_collision &m q:
  0 <= q
  =>    Pr[EF_CMA(Wrap(SS), Reduction(AS)).main() @ &m:
               (Reduction.unc, Reduction.ncc) \in Wrap.qs
            /\ card Wrap.qs <= q]
     <= q%r * mu1 dnonce witness.
proof.
move=> ge0_q; byphoare=> //=.
conseq (: _ ==> Reduction.unc \in image fst Wrap.qs /\ card Wrap.qs <= q)=> //=.
+ by move=> ncc unc qs /> nnc_in_qs _; rewrite imageP; exists (unc, ncc).
proc; inline *.
seq 19: (card Wrap.qs <= q) 1%r (q%r * mu1 dnonce witness) _ 0%r=> //=.
+ seq  1: (Reduction.unc \in image fst Wrap.qs) (q%r * mu1 dnonce witness) 1%r _ 0%r=> //=.
  + rnd; skip=> /> &hr qs_le_q; rewrite (Mu_mem.mu_mem _ _ (mu1 dnonce witness)).
    + by move=> x _; exact/dnonce_funi.
    apply/ler_pmul2r.
    + by exact/dnonce_fu.
    exact/le_fromint/(lez_trans _ _ _ _ qs_le_q)/fcard_image_leq.
  by conseq (: _ ==> false)=> //= &hr ->.
by hoare; conseq (: _ ==> true)=> //= &hr ->.
qed.

end section Security_Proof.
end Basic.

theory Modify.

module type Adv_BSec (O : Oracle) = {
  proc start(): real * sid { O.set_location }
  proc phase1(pk : pkey): unit
     { O.get_clock, O.set_clock, O.get_location, O.set_location,
       O.honest_transition, O.honest_action,
       O.adversary_send, O.adversary_modify, O.adversary_read }
  proc main(_ : mhandle): ctime * nonce * signature
     { O.get_clock, O.set_clock, O.get_location,
       O.honest_action,
       O.adversary_send, O.adversary_modify, O.adversary_read }
  proc main_Part1(_ : mhandle): ctime * bool * (mhandle option)
     { O.get_clock, O.set_clock, O.get_location,
       O.honest_action,
       O.adversary_send, O.adversary_modify, O.adversary_read }
  proc main_Part2(_ : mhandle option): ctime * nonce * signature
     { O.get_clock, O.set_clock, O.get_location,
       O.honest_action,
       O.adversary_send, O.adversary_modify, O.adversary_read }
}.

(* Basic Security with Modify Experiment *)
module Exp_Adv_BMSec (A : Adv_BSec) (C : Card) (R : Reader) = {
  proc main(): bool ={
    var tc1, tc2, bound, sidc, mho, mh, pk, qcard, ro, ncc, sigc;
    var o   <- None;
    var rho <- None;
    var resp <- None;

    Oracle(C, R).init();

    (bound, sidc) <@ A(Oracle(C, R)).start();
               pk <@ Oracle(C, R).setup(bound);

    A(Oracle(C, R)).phase1(pk);

    if (bound < 2%r * dl (oget Env.lmap.[Reader]) (oget Env.lmap.[Card])) {
      (mho, o) <@ Oracle(C, R).honest_transition(TReaderInit sidc);

      if (mho <> None) {
        (tc1, qcard, rho) <@ A(Oracle(C,R)).main_Part1(oget mho);

        Env.set_clock(tc1);
        if (qcard) {
          Env.set_clock(dl (oget Env.lmap.[Reader]) (oget Env.lmap.[Card]));
          (resp, ro) <@ Oracle(C,R).honest_transition(TCard (oget mho) rho);
          Env.set_clock(dl (oget Env.lmap.[Adv]) (oget Env.lmap.[Card]));
        }

        (tc2, ncc, sigc) <@ A(Oracle(C,R)).main_Part2(resp);

        Env.set_clock(tc2);
        mh <@ Oracle(C, R).adversary_send(Response ncc sigc);

        Env.set_clock(dl (oget Env.lmap.[Adv]) (oget Env.lmap.[Reader]));
        (mho, o) <@ Oracle(C, R).honest_transition(TReaderFinal sidc mh None);
      }
    }
    return o = Some Accept;
  }
}.

(** The Reduction : cast reader, card and experiment as
    an adversary against the signing scheme, constrained by the environment  **)
module Reader0 (S : PScheme) : Reader = {
  proc init(b : real, pk : pkey) : unit = {
    Reader.bound <- b;
     Reader.smap <- empty;
      Reader.cpk <- pk;
  }

  proc generateUN() : ctime * nonce = {
    var t, un;

     t <@ Env.get_clock();
    un <$ dnonce;
    return (t, un);
  }

  proc verify(st : ctime, sn : nonce, nc : nonce, sig : signature) : out = {
    var t, r, ev;

    t <@ Env.get_clock();
    if (dt t st <= Reader.bound) {
      ev <@ S.verify((sn, nc), sig);
       r <- if ev then Accept else Reject;
    } else {
      r <- Reject;
    }
    return r;
  }

  proc send_challenge(sid : sid) : message option * out option = {
    var t, un;
    var r <- None;

    if (sid \notin Reader.smap) {
                (t, un) <@ generateUN();
      Reader.smap.[sid] <- (t, un);
                      r <- Some (Challenge un);
    }
    return (r, None);
  }

  proc recv_response(sid : sid, m : message) : message option * out option = {
    var st, sn, ro;
    var r <- None;

    match Reader.smap.[sid] with
    | None          => { }
    | Some s => {
      (st, sn) <- s;
      match m with
      | Challenge _      => { }
      | Response  nc sig => {
          ro <@ verify(st, sn, nc, sig);
           r <- Some ro;
        }
      end;
      Reader.smap <- rem Reader.smap sid;
      }
    end;
    return (None, r);
  }

  proc timedout(): unit = {
    var t;

              t <@ Env.get_clock();
    Reader.smap <- filter (fun x (y : _ * _), dt t y.`1 <= Reader.bound) Reader.smap;
  }
}.

module Card0 (S : PScheme) : Card = {
  proc init() : pkey = {
    return witness;
  }

  proc generateNC() : nonce = {
    var nc;

    nc <$ dnonce;
    return nc;
  }

  proc certify(un : nonce) : nonce * signature = {
    var nc, sig;

    nc  <@ generateNC();
    sig <@ S.sign(un, nc);
    return (nc, sig);
  }

  proc recv_challenge(m : message) : message option * out option = {
    var ro;
    var r <- None;

    match m with
    | Challenge nr  => {
        ro <@ certify(nr);
         r <- Some (Response ro.`1 ro.`2);
      }
    | Response  _ _ => { }
    end;
    return (r, None);
  }
}.

module ReductionMod (A : Adv_BSec) (S : PScheme) = {
  var pk   : pkey

  module O : Oracle = {
    proc init() = {
      Env.init();
    }

    proc setup(bound) : pkey = {
      Reader0(S).init(bound, pk);
      return pk;
    }

    proc get_clock = Env.get_clock
    proc set_clock = Env.set_clock

    proc get_location = Env.get_location
    proc set_location = Env.set_location

    proc honest_action(a : action) = {
      match a with
      | Timeout => {
          Reader0(S).timedout();
        }
      end;
    }

    proc sendo(p, m : message option) = {
      var h;
      var ho <- None;

      match m with
      | None   => { }
      | Some m => {
          h <@ Env.send(p, m);
          ho <- Some h;
        }
      end;
      return ho;
    }

    proc honest_transition(tr : transition) = {
      var p;
      var m  <- None;
      var r  <- None;
      var ho <- None;

      match tr with
      | TCard mh rh => {
          p <- Card;
          m <@ Env.recv(p, mh, rh);
          match m with
          | None    => { }
          | Some m' => {
              (m, r) <@ Card0(S).recv_challenge(m');
            }
          end;
        }
      | TReaderInit sid => {
               p <- Reader;
          (m, r) <@ Reader0(S).send_challenge(sid);
        }
      | TReaderFinal sid mh rh => {
          p <- Reader;
          m <@ Env.recv(p, mh, rh);
          match m with
          | None    => { }
          | Some m' => {
              (m, r) <@ Reader0(S).recv_response(sid, m');
            }
          end;
        }
      end;
      ho <@ sendo(p, m);

      return (ho, r);
    }

    proc adversary_read(h : mhandle, rh : mhandle option) : message option = {
      var m;

      m <@ Env.recv(Adv, h, rh);
      return m;
    }

    proc adversary_modify(f : message -> message) : mhandle = {
      var h;

      h <@ Env.modify(Adv, f);
      return h;
    }

    proc adversary_send(m : message) : mhandle = {
      var h;

      h <@ Env.send(Adv, m);
      return h;
    }
  }

  var unc  : nonce
  var ncc  : nonce
  var sigc : signature

  proc forge(pk0) = {
    var tc1, tc2, qcard, rho, bound, sidc, h;

    pk <- pk0;
          O.init();

    (bound, sidc) <@ A(O).start();

    O.setup(bound);

    A(O).phase1(pk);

    unc <$ dnonce;
      h <@ Env.send(Reader, Challenge unc);

    (tc1, qcard, rho) <@ A(O).main_Part1(h);
    Env.set_clock(tc1);

    (tc2, ncc, sigc) <@ A(O).main_Part2(None);

    (** If the adversary makes the BSec experiment return true,
        then its output is a valid forgery unless (unc, ncc) \in qs **)

    return ((unc, ncc), sigc);
  }
}.

section Security_Proof.
declare module SS <: Scheme  { -Env, -Card, -Reader, -Oracle, -Wrap, -ReductionMod }.

declare axiom SS_kll: islossless SS.keygen.
declare axiom SS_sll: islossless SS.sign.
declare axiom SS_vll: islossless SS.verify.

declare module AS <: Adv_BSec { -Env, -Card, -Reader, -Oracle, -Wrap, -ReductionMod, -SS }.

axiom AS_mll (O <: Oracle { -AS }):
     islossless O.get_clock
  => islossless O.set_clock
  => islossless O.get_location
  => islossless O.honest_action
  => islossless O.adversary_read
  => islossless AS(O).main.

axiom AM1_ll (O <: Oracle { -AS }):
     islossless O.get_clock
  => islossless O.set_clock
  => islossless O.get_location
  => islossless O.honest_action
  => islossless O.adversary_read
  => islossless O.adversary_modify
  => islossless O.adversary_send
  => islossless AS(O).main_Part1.

axiom AM2_ll (O <: Oracle { -AS }):
     islossless O.get_clock
  => islossless O.set_clock
  => islossless O.get_location
  => islossless O.honest_action
  => islossless O.adversary_read
  => islossless O.adversary_modify
  => islossless O.adversary_send
  => islossless AS(O).main_Part2.

local equiv DirectMod :
  Exp_Adv_BMSec(AS, Card(SS), Reader(SS)).main ~ EF_CMA(Wrap(SS), ReductionMod(AS)).main:
    ={glob AS, glob SS}
    ==> (!(ReductionMod.unc, ReductionMod.ncc) \in Wrap.qs){2} => (res{1} => res{2}).
proof.
proc.
inline ReductionMod(AS,Wrap(SS)).forge.
seq  7  7: (   ={glob AS, glob Env, glob SS, glob Reader}
            /\ ={sk}(Card,Wrap)
            /\ Reader.cpk{1} = ReductionMod.pk{2}
            /\ Reader.cpk{1} = Wrap.pk{2}
            /\ Reader.cpk{1} = pk{2}
            /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
            /\ o{1} = None
            /\ resp{1} = None
            /\ bound{1} = Reader.bound{1}).
+ inline *; auto.
  call (:    ={glob Env, glob SS, glob Reader}
          /\ ={sk}(Card,Wrap)
          /\ Reader.cpk{1} = ReductionMod.pk{2}
          /\ Reader.cpk{1} = Wrap.pk{2}
          /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}).
  + by conseq (: ={res}) _ wfe_get_clock=> //; sim.
  + by conseq (: ={Env.clock}) _ wfe_set_clock=> //; sim.
  + by conseq (: ={res}) _ wfe_get_location=> //; sim.
  + by conseq (: ={Env.lmap}) _ wfe_set_location=> //; sim.
  + conseq (:    ={glob Env, glob SS, glob Reader, res}
              /\ ={sk}(Card, Wrap)
              /\ Reader.cpk{1} = ReductionMod.pk{2}
              /\ Reader.cpk{1} = Wrap.pk{2})
           _
           (: well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              ==> well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> |>.
    + proc. inline ReductionMod(AS, Wrap(SS)).O.sendo.
      seq  7: (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock); last first.
      + exists * m0; elim *=> - [|m].
        + by match None 1; auto.
        match Some 1; first by auto=> /#.
        by auto; call wfe_send; auto.
      exists * tr; elim * => - [mh rh|sid|sid0 mh rh].
      + match TCard 4; first by auto=> /#.
        seq  5: (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock).
        + by call wfe_recv; auto.
        exists * m; elim * => - [|m].
        + by match None 1; auto.
        match Some 1; first by auto=> /#.
        inline *; case @[ambient]: m=> [nc|nr sig].
        + match Challenge 3.
          + by auto=> /> _ _ _ _ _ _; exists nc.
          by auto; call (: true); auto.
        match Response 3.
        + by auto=> /> _ _ _ _ _ _; exists nr sig.
        by auto.
      + match TReaderInit 4; first by auto=> /#.
        by inline *; sp; if; auto.
      match TReaderFinal 4; first by auto=> /#.
      seq  5: (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock).
      + by call wfe_recv; auto.
      exists * m; elim * => - [|m].
      + by match None 1; auto.
      match Some 1; first by auto=> /#.
      inline *. exists * Reader.smap.[sid]; elim * => - [|stc].
      + match None 4; first by auto=> /#.
        by auto.
      match Some 4; first by auto=> /#.
      case @[ambient]: m=> [nc|nr sig].
      + match Challenge 6.
        + by auto=> /> _ _ _ _ _ _ _; exists nc.
        by auto.
      match Response 6.
      + by auto=> /> _ _ _ _ _ _ _; exists nr sig.
      by sp; if; auto; call (: true); auto.
    proc; inline Env.get_clock.
    sim; sp; match=> //= [mh1 rh1 mh2 rh2|sid1 sid2|sid1 mh1 rh1 sid2 mh2 rh2].
    + seq  2  2: (   ={glob Env, glob SS, glob Reader, m, p, r, ho}
                  /\ ={sk}(Card,Wrap)
                  /\ Reader.cpk{1} = ReductionMod.pk{2}
                  /\ Reader.cpk{1} = Wrap.pk{2}
                  /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}).
      + call (: ={glob Env, arg} ==> ={res}); first by sim.
        by auto.
      match=> //= m1 m2.
      seq  1  1: (   ={glob Env, glob SS, glob Reader, m, p, r, ho}
                  /\ ={sk}(Card,Wrap)
                  /\ Reader.cpk{1} = ReductionMod.pk{2}
                  /\ Reader.cpk{1} = Wrap.pk{2}
                  /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2})=> //=.
      call (:    ={glob Env, glob SS, arg}
              /\ ={sk}(Card,Wrap)
              ==> ={glob SS, res}).
      + proc; sp; match=> //= n1 n2.
        by inline *; sim; call (: true); auto.
      by auto.
    + seq  2  2: (   ={glob Env, glob SS, glob Reader, m, p, r, ho}
                  /\ ={sk}(Card,Wrap)
                  /\ Reader.cpk{1} = ReductionMod.pk{2}
                  /\ Reader.cpk{1} = Wrap.pk{2}
                  /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2})=> //=.
      call (: ={glob Env, glob Reader, arg}
              ==> ={glob Reader, res}); first by sim.
      by auto.
    seq  2  2: (   ={glob Env, glob SS, glob Reader, m, p, r, ho}
                /\ ={sk}(Card,Wrap)
                /\ Reader.cpk{1} = ReductionMod.pk{2}
                /\ Reader.cpk{1} = Wrap.pk{2}
                /\ sid1 = sid2
                /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2})=> //=.
    + call (: ={glob Env, glob Reader, arg}
              ==> ={glob Reader, res}); first by sim.
      by auto.
    match=> //= m1 m2.
    call (:    ={glob SS, glob Reader, arg}
            /\ Reader.cpk{1} = Wrap.pk{2}
            ==> ={glob SS, glob Reader, res}).
    + proc; inline Env.get_clock.
      sp; match=> //= st1 st2; sp; match=> //= [nc1 nc2|nr1 sig1 nr2 sig2].
      + by auto.
      wp; inline *; sp; if=> /> => [+ + ->||] //; auto.
      by wp; call (: true); auto=> /> + + ->.
    by auto.
  + by conseq (: ={glob Reader})=> //; sim.
  + conseq (: ={glob Env, res}) _
           (: well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              ==> well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> |>.
    + by proc; call wfe_send.
    by sim.
  + conseq (: ={glob Env, res}) _
           (:  well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              ==> well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> |>.
    + by proc; call wfe_modify.
    by sim.
  + by conseq (: ={res})=> //; sim.
  inline *. swap{1} [12..13] -11.
  wp; call (: ={glob Env} /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}).
  + by conseq (: ={glob Env}) _ wfe_set_location=> //; sim.
  wp; call (: true); auto=> //=.
  auto=> |>; do !split=> //.
  + by move=> mh; apply/WFmh_None; smt(emptyE).
  by move=> rh; apply/WFrh_None; smt(emptyE).
(* if bound < 2* dl = (R,C) *)
if{1}; last first.
+ conseq (: _ ==> true) (: o = None ==> o = None)=> //=.
  conseq _ _ (: _ ==> true).
  islossless.
  + exact/SS_vll.
  + by apply/(AM2_ll (<: ReductionMod(AS, Wrap(SS)).O) _); islossless; inline *; auto=> /#.
  by apply/(AM1_ll (<: ReductionMod(AS, Wrap(SS)).O) _); islossless; inline *; auto=> /#.
case: (Reader.smap.[sidc]{1} = None); last first.
+ inline {1} 1. inline {1} 6.
  match TReaderInit {1} 5; first by auto=> /#.
  inline {1} 7.
  rcondf {1}  9; first by auto; rewrite /get_as_TReaderInit.
  match None {1} 13; first by auto.
  rcondf {1} 15; first by auto.
  auto=> //=.
  conseq _ _ (: _ ==> true).
  islossless.
  + exact/SS_vll.
  + by apply/(AM2_ll (<: ReductionMod(AS, Wrap(SS)).O) _); islossless; inline *; auto=> /#.
  by apply/(AM1_ll (<: ReductionMod(AS, Wrap(SS)).O) _); islossless; inline *; auto=> /#.
rcondt{1} 2.
+ auto; inline *.
  match TReaderInit  5; first by auto=> /#.
  rcondt  9; first by auto=> /#.
  match Some 18; first by auto=> /#.
  by auto.
exists * Env.clock{1}; elim * => t0.
seq  1  2: (   ={glob AS, glob Env, glob SS, Reader.bound}
            /\ ={sk}(Card, Wrap)
            /\ Reader.cpk{1} = ReductionMod.pk{2}
            /\ Reader.cpk{1} = Wrap.pk{2}
            /\ Reader.cpk{1} = pk{2}
            /\ Env.nmap.[h]{2} = Some (t0, oget Env.lmap.[Reader]{1}, Challenge (ReductionMod.unc{2}))
            /\ Reader.smap.[sidc]{1} = Some (t0, ReductionMod.unc{2})
            /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
            /\ o{1} = None
            /\ resp{1} = None
            /\ t0 = Env.clock{1}
            /\ mho{1} = Some h{2}
            /\ Reader.bound{1} < 2%r * dl (oget Env.lmap{1}.[Reader]) (oget Env.lmap{1}.[Card])).
+ inline Env.get_clock Env.send Oracle(Card(SS), Reader(SS)).sendo
         Reader(SS).send_challenge Reader(SS).generateUN
         Oracle(Card(SS), Reader(SS)).honest_transition.
  match TReaderInit {1} 5; first by auto=> /#.
  rcondt{1}  9; first by auto=> /#.
  match Some {1} 18; first by auto=> /#.
  auto=> @/get_as_Some @/get_as_TReaderInit |> &1 &2 venv _ _ n _.
  rewrite !get_set_sameE /=; do !split=> //=; first 3 by case: venv; smt().
  + move=> mh; case: (mh = Env.mh{2})=> [->>|neq_mh].
    + apply/(WFmh_Some _ _ _ _ t0 (oget Env.lmap.[Reader]){2} (Challenge n)).
      + by rewrite get_set_sameE.
      + by case: venv; smt().
      by case: venv; smt().
    move: venv => @/well_formed_env [#] _ _ _ wfm_inv _.
    case: (wfm_inv mh)=> [t l m nmap_mh mh_bd t_bd|nmap_mh mh_bd].
    + by apply/(WFmh_Some _ _ _ _ t l m); rewrite ?get_set_neqE //#.
    by apply/WFmh_None; rewrite ?get_set_neqE //#.
  by move: venv => @/well_formed_env [#] _ _ _ _.
(* deal with A.main_part1 *)
seq  1  1: (   ={glob AS, glob Env, glob SS, Reader.bound, tc1, qcard, rho}
            /\ ={sk}(Card,Wrap)
            /\ Reader.cpk{1} = ReductionMod.pk{2}
            /\ Reader.cpk{1} = Wrap.pk{2}
            /\ Reader.cpk{1} = pk{2}
            /\ Env.nmap.[h]{2} = Some (t0, oget Env.lmap.[Reader]{1}, Challenge ReductionMod.unc{2})
            /\ (Reader.smap.[sidc]{1} <> None => Reader.smap.[sidc]{1} = Some (t0, ReductionMod.unc{2}))
            /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
            /\ o{1} = None
            /\ resp{1} = None
            /\ t0 <= Env.clock{1}
            /\ mho{1} = Some h{2}
            /\ Reader.bound{1} < 2%r * dl (oget Env.lmap{1}.[Reader]) (oget Env.lmap{1}.[Card])).
+ exists * h{2}, Env.nmap.[h]{2}; elim * => mh [|[] t0' lReader nc].
  + by exfalso=> /#.
  exists * sidc{1}, Reader.smap.[sidc]{1}; elim * => sidc [|state].
  + by exfalso=> /#.
  call (:    ={glob Env, glob SS, Reader.bound}
          /\ ={sk}(Card,Wrap)
          /\ Reader.cpk{1} = ReductionMod.pk{2}
          /\ Reader.cpk{1} = Wrap.pk{2}
          /\ Env.nmap{2}.[mh] = Some (t0', lReader, nc)
          /\ (Reader.smap{1}.[sidc] <> None => Reader.smap{1}.[sidc] = Some state)
          /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
          /\ t0 <= Env.clock{1}).
  + by conseq (: ={res}) _ wfe_get_clock=> //; sim.
  + conseq (: ={glob Env} /\ t0 <= Env.clock{1}) _ wfe_set_clock=> //.
    by proc; inline *; auto=> |> /#.
  + by conseq (: ={res})=> //; sim.
  + conseq (: true)
           (: Reader.smap.[sidc] <> None => Reader.smap.[sidc] = Some state
              ==> Reader.smap.[sidc] <> None => Reader.smap.[sidc] = Some state)=> //.
    + proc; exists * a; elim * => - [].
      match Timeout 1; inline *; auto=> |> &1.
      rewrite !filterE //=; case: (Reader.smap{1}.[sidc])=> |>.
      by case: (dt Env.clock{1} state.`1 <= Reader.bound{1}).
    by sim.
  + conseq (: ={glob Env, res}) _
           (:    well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              /\ Env.nmap.[mh] = Some (t0', lReader, nc)
              ==>    well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                  /\ Env.nmap.[mh] = Some (t0', lReader, nc))=> |>.
    + proc; call (:    well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                    /\ Env.nmap.[mh] = Some (t0', lReader, nc)
                    ==>    well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                        /\ Env.nmap.[mh] = Some (t0', lReader, nc))=> //.
      conseq wfe_send (:    well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                         /\ Env.nmap.[mh] = Some (t0', lReader, nc)
                         ==> Env.nmap.[mh] = Some (t0', lReader, nc))=> //.
      proc; auto=> |> &1 wfe_inv nmap_mh; rewrite get_set_neqE //.
      by move: wfe_inv=> @/well_formed_env [#] _ _ _ + _ - /(_ mh) []; rewrite nmap_mh //#.
    by sim.
  + conseq (: ={glob Env, res}) _
           (: well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              ==> well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> |>.
    + by proc; call wfe_modify.
    by sim.
  + by conseq (: ={res})=> //; sim.
  by auto=> /#.
exists * Env.clock{1}; elim * => t1.
seq  1  1: (   ={glob AS, glob Env, glob SS, Reader.bound, tc1, qcard, rho}
            /\ ={sk}(Card, Wrap)
            /\ Reader.cpk{1} = ReductionMod.pk{2}
            /\ Reader.cpk{1} = Wrap.pk{2}
            /\ Reader.cpk{1} = pk{2}
            /\ Env.nmap{2}.[h{2}] =
                  Some (t0, oget Env.lmap{1}.[Top.Reader], Challenge ReductionMod.unc{2})
            /\ (Reader.smap.[sidc]{1} <> None => Reader.smap{1}.[sidc{1}] = Some (t0, ReductionMod.unc{2}))
            /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
            /\ o{1} = None
            /\ resp{1} = None
            /\ mho{1} = Some h{2}
            /\ Reader.bound{1} < 2%r * dl (oget Env.lmap{1}.[Reader]) (oget Env.lmap{1}.[Card])
            /\ t0 <= t1
            /\ Env.clock{1} = t1 + if 0%r <= tc1{1} then tc1{1} else 0%r).
+ conseq (: ={tc1, Env.clock} ==> ={Env.clock}) _
         (:    t1 = Env.clock
            /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
            ==>    (Env.clock = t1 + if 0%r <= tc1 then tc1 else 0%r)
                /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock))=> //.
  + exists * tc1; elim * => tick.
    call (: tick = t /\ t1 = Env.clock /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
            ==>    (Env.clock = t1 + if 0%r <= tick then tick else 0%r)
                /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> //.
    by conseq (sem_set_clock tick t1) wfe_set_clock=> |> &1; case: (0%r <= tick).
  by sim.
exists * Env.clock{1}; elim * => t2.
seq  1  0: (   ={glob AS, Reader.bound, tc1, qcard, rho}
            /\ ={sk}(Card, Wrap)
            /\ Reader.cpk{1} = ReductionMod.pk{2}
            /\ Reader.cpk{1} = Wrap.pk{2}
            /\ Reader.cpk{1} = pk{2}
            /\ Env.nmap{2}.[h{2}] =
                  Some (t0, oget Env.lmap{1}.[Top.Reader], Challenge ReductionMod.unc{2})
            /\ (Reader.smap.[sidc]{1} <> None => Reader.smap{1}.[sidc{1}] = Some (t0, ReductionMod.unc{2}))
            /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
            /\ o{1} = None
            /\ mho{1} = Some h{2}
            /\ Reader.bound{1} < 2%r * dl (oget Env.lmap{1}.[Reader]) (oget Env.lmap{1}.[Card])
            /\ t0 <= t1 <= t2
            /\ t2 = t1 + (if 0%r <= tc1 then tc1 else 0%r){1}
            /\ if qcard{1}
               then  Env.clock{1} = t2 + dl (oget Env.lmap{1}.[Reader]) (oget Env.lmap{1}.[Card])
                                       + dl (oget Env.lmap{1}.[Adv]) (oget Env.lmap{1}.[Card])
               else    ={glob Env, glob SS}
                    /\ Env.clock{1} = t2
                    /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
                    /\ resp{1} = None
          ).
+ if{1}; last first.
  + by auto=> /#.
  seq  1  0: (   ={glob AS, Env.nmap, Env.mmap, Env.lmap, glob SS, Reader.bound, tc1, qcard, rho}
              /\ ={sk}(Card, Wrap)
              /\ Reader.cpk{1} = ReductionMod.pk{2}
              /\ Reader.cpk{1} = Wrap.pk{2}
              /\ Reader.cpk{1} = pk{2}
              /\ Env.nmap{2}.[h{2}] =
                    Some (t0, oget Env.lmap{1}.[Top.Reader], Challenge ReductionMod.unc{2})
              /\ (Reader.smap.[sidc]{1} <> None => Reader.smap{1}.[sidc{1}] = Some (t0, ReductionMod.unc{2}))
              /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
              /\ o{1} = None
              /\ mho{1} = Some h{2}
              /\ Reader.bound{1} < 2%r * dl (oget Env.lmap{1}.[Reader]) (oget Env.lmap{1}.[Card])
              /\ t0 <= t1 <= t2
              /\ t2 = t1 + (if 0%r <= tc1 then tc1 else 0%r){1}
              /\ Env.clock{1} = t2 + dl (oget Env.lmap.[Reader]){1} (oget Env.lmap.[Card]){1}
              /\ qcard{1}).
  + by inline Env.set_clock; auto=> |>; rewrite ge0_dl /#.
  inline Oracle(Card(SS), Reader(SS)).honest_transition.
  match TCard {1} 5; first by auto=> /#.
  inline Env.recv.
  match Some {1} 13.
  + auto=> @/get_as_TCard /> + -> //= - _ _ _ _ _ _ _ _ _ _ _.
    by exists (t0, oget Env.lmap.[Top.Reader], Challenge ReductionMod.unc){m}.
  rcondt{1} 14.
  + auto => &1 @/get_as_TCard @/get_as_Some |> nmap_h smap_sidc Hfar.
    by rewrite nmap_h=> |>; smt(dl_sym ge0_dl).
  exists * rho{1}; elim * => - [|rh].
  + match None {1} 15; first by auto=> /#.
    match Some {1} 16; first by auto=> /#.
    inline *.
    match Challenge {1} 19.
    + auto=> @/get_as_TCard @/get_as_Some /> + -> /> - _ _ _ _ _ _ _ _ _ _ _.
      by exists ReductionMod.unc{m}.
    match Some {1} 30.
    + by auto; call (: true); auto=> /#.
    by auto; call{1} SS_sll; auto; rewrite ge0_dl.
  match Some {1} 15.
  + by auto=> @/get_as_TCard /> _ _ _ _ _ _ _ _ _ _ _ _; exists rh.
  exists * Env.mmap.[oget rho]{1}; elim * => - [|tlf].
  + match None {1} 16.
    + by auto=> @/get_as_TCard @/get_as_Some /> + <-.
    match Some {1} 17; first by auto=> /#.
    inline Card(SS).recv_challenge.
    match Challenge {1} 20.
    + auto=> @/get_as_Some @/get_as_TCard /> + + -> /> - _ _ _ _ _ _ _ _ _ _ _ _.
      by exists ReductionMod.unc{m}.
    inline Oracle(Card(SS), Reader(SS)).sendo.
    match Some {1} 27.
    + by auto; inline *; wp; call (: true); auto=> /#.
    inline *.
    by wp; call {1} SS_sll; auto=> |> &1 &2; rewrite ge0_dl.
  match Some {1} 16.
  + auto=> @/get_as_Some @/get_as_TCard /> + <- - _ _ _ _ _ _ _ _ _ _ _ _.
    by exists tlf.
  match Some {1} 19; first by auto=> /#.
  inline *.
  seq 21  0: (   ={glob AS, Env.nmap, Env.mmap, Env.lmap, glob SS, Reader.bound, tc1, qcard, rho}
              /\ ={sk}(Card, Wrap)
              /\ Reader.cpk{1} = ReductionMod.pk{2}
              /\ Reader.cpk{1} = Wrap.pk{2}
              /\ Reader.cpk{1} = pk{2}
              /\ Env.nmap{2}.[h{2}] =
                    Some (t0, oget Env.lmap{1}.[Top.Reader], Challenge ReductionMod.unc{2})
              /\ (Reader.smap.[sidc]{1} <> None => Reader.smap{1}.[sidc{1}] = Some (t0, ReductionMod.unc{2}))
              /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
              /\ o{1} = None
              /\ mho{1} = Some h{2}
              /\ Reader.bound{1} < 2%r * dl (oget Env.lmap{1}.[Reader]) (oget Env.lmap{1}.[Card])
              /\ t0 <= t1 <= t2
              /\ t2 = t1 + (if 0%r <= tc1 then tc1 else 0%r){1}
              /\ Env.clock{1} = t2 + dl (oget Env.lmap.[Reader]){1} (oget Env.lmap.[Card]){1}
              /\ qcard{1}).
  + by auto.
  exists * m0{1}; elim * => - [nc|nr sig].
  + match Challenge {1} 1; first by auto=> /#.
    by inline *; wp; call {1} SS_sll; auto=> |>; rewrite ge0_dl.
  match Response {1} 1; first by auto=> /#.
  by auto=> |>; rewrite ge0_dl /#.

exists * qcard{1}; elim * => - [].
+ seq 1 1: (   ={Reader.bound, tc1, qcard, rho}
            /\ ={sk}(Card, Wrap)
            /\ Reader.cpk{1} = ReductionMod.pk{2}
            /\ Reader.cpk{1} = Wrap.pk{2}
            /\ Reader.cpk{1} = pk{2}
            /\ Env.nmap{2}.[h{2}] =
                  Some (t0, oget Env.lmap{1}.[Top.Reader], Challenge ReductionMod.unc{2})
            /\ (Reader.smap.[sidc]{1} <> None => Reader.smap{1}.[sidc{1}] = Some (t0, ReductionMod.unc{2}))
            /\ o{1} = None
            /\ t0 <= t1 <= t2
            /\ t2 = t1 + (if 0%r <= tc1{1} then tc1{1} else 0%r)
            /\ (   t2 + dl (oget Env.lmap.[Reader]) (oget Env.lmap.[Card])
                      + dl (oget Env.lmap.[Adv]) (oget Env.lmap.[Card])
                <= Env.clock){1}
            /\ Reader.bound{1} < 2%r * dl (oget Env.lmap{1}.[Reader]) (oget Env.lmap{1}.[Card])).
  + exists * h{2}, Env.nmap.[h]{2}; elim * => mh [|[] t0' lReader nc].
    + by exfalso=> /#.
    exists * sidc{1}, Reader.smap.[sidc]{1}; elim * => sidc smap_sid.
    call {2} (:    Env.nmap.[mh] = Some (t0', lReader, nc)
                /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                ==>    Env.nmap.[mh] = Some (t0', lReader, nc)
                    /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock).
    + conseq (: true ==> true)
             (:    Env.nmap.[mh] = Some (t0', lReader, nc)
                /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                ==>    Env.nmap.[mh] = Some (t0', lReader, nc)
                    /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> //.
      + proc (   Env.nmap.[mh] = Some (t0', lReader, nc)
              /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> //.
        + by conseq wfe_get_clock.
        + by conseq wfe_set_clock.
        + by conseq wfe_get_location.
        + by proc; exists * a; elim * => - []; match Timeout 1; inline *; auto.
        + proc. call (:    Env.nmap.[mh] = Some (t0', lReader, nc)
                        /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                        ==>    Env.nmap.[mh] = Some (t0', lReader, nc)
                            /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> //.
          conseq wfe_send (: Env.nmap.[mh] = Some (t0', lReader, nc))=> //.
          proc; auto=> &1 |> nmap_mh wfe_inv; rewrite get_set_neqE //.
          by move: wfe_inv=> @/well_formed_env [#] _ _ _ + _ - /(_ mh) []; rewrite nmap_mh //#.
        + by proc; call wfe_modify.
        by proc; call wfe_recv.
      by apply/(AM2_ll (<: ReductionMod(AS, Wrap(SS)).O))=> //; first 7 by (islossless; inline *; auto=> /#).
    call {1} (:    (Reader.smap.[sidc] <> None => Reader.smap.[sidc] = smap_sid)
                /\    t2 + dl (oget Env.lmap.[Reader]) (oget Env.lmap.[Card])
                         + dl (oget Env.lmap.[Adv]) (oget Env.lmap.[Card])
                   <= Env.clock
                ==>    (Reader.smap.[sidc] <> None => Reader.smap.[sidc] = smap_sid)
                    /\    t2 + dl (oget Env.lmap.[Reader]) (oget Env.lmap.[Card])
                             + dl (oget Env.lmap.[Adv]) (oget Env.lmap.[Card])
                       <= Env.clock)=> //.
    + conseq (: true ==> true)
             (:    (Reader.smap.[sidc] <> None => Reader.smap.[sidc] = smap_sid)
                /\    t2 + dl (oget Env.lmap.[Reader]) (oget Env.lmap.[Card])
                         + dl (oget Env.lmap.[Adv]) (oget Env.lmap.[Card])
                   <= Env.clock
                ==>    (Reader.smap.[sidc] <> None => Reader.smap.[sidc] = smap_sid)
                    /\    t2 + dl (oget Env.lmap.[Reader]) (oget Env.lmap.[Card])
                             + dl (oget Env.lmap.[Adv]) (oget Env.lmap.[Card])
                       <= Env.clock)=> //.
      + proc (   (Reader.smap.[sidc] <> None => Reader.smap.[sidc] = smap_sid)
              /\    t2 + dl (oget Env.lmap.[Reader]) (oget Env.lmap.[Card])
                       + dl (oget Env.lmap.[Adv]) (oget Env.lmap.[Card])
                 <= Env.clock)=> //.
        + by conseq (: true).
        + by proc; auto=> /#.
        + by conseq (: true).
        + proc; exists * a; elim * => - [].
          match Timeout 1; inline *; auto=> |> &1.
          rewrite !filterE //=; case: (Reader.smap{1}.[sidc])=> |> x.
          by case: (dt Env.clock{1} x.`1 <= Reader.bound{1}).
        + by conseq (: true).
        + by conseq (: true).
        by conseq (: true).
      by apply/(AM2_ll (<: Oracle(Card(SS), Reader(SS))) _)=> //; first 7 by (islossless; inline *; auto=> /#).
    auto=> /> + + <- />.
    by auto=> /#.
  inline Env.set_clock.
  inline Oracle(Card(SS), Reader(SS)).adversary_send Env.send
         Oracle(Card(SS), Reader(SS)).honest_transition.
  match TReaderFinal {1} 19; first by auto=> /#.
  inline Env.recv.
  match Some {1} 27.
  + auto=> @/get_as_TReaderFinal />; rewrite get_set_sameE=> //= &0 _ _ _ _ _ _.
    by exists (Env.clock + if 0%r <= tc2 then tc2 else 0%r, oget Env.lmap.[Adv], Response ncc sigc){0}.
  rcondt{1} 28.
  + auto=> &1 @/get_as_TReaderFinal @/get_as_Some |>; rewrite !get_set_sameE ge0_dl //=.
    smt(dl_sym).
  match None {1} 29; first by auto=> /#.
  match Some {1} 30; first by auto=> /#.
  inline Reader(SS).recv_response Env.get_clock.
  exists * Reader.smap.[sidc]{1}; elim * => - [|state].
  + match None {1} 34.
    + by auto=> @/get_as_TReaderFinal /> + <-.
    inline Oracle(Card(SS), Reader(SS)).sendo.
    match None {1} 38; first by auto=> /#.
    by inline *; wp; call {2} SS_vll; auto.
  match Some {1} 34.
  + by auto=> @/get_as_TReaderFinal /> + <- - _ _ _ _ _ _ _; exists state.
  match Response {1} 36.
  + auto=> &1 @/get_as_TReaderFinal @/get_as_Some |> _ _ _ _ _ _ _.
    by exists ncc{1} sigc{1}; rewrite !get_set_sameE.
  inline Reader(SS).verify Env.get_clock.
  rcondf{1} 42.
  + auto=> @/get_as_TReaderFinal @/get_as_Some />.
    move=> &0 <- /> nmap_h le_t0_t1 le_t1_t1T le_t1T_clock Hfar.
    smt(dl_leA dl_sym ge0_dl).
  conseq (: o{1} = Some Reject)=> |>.
  inline *; auto=> |>.
  by call {2} SS_vll; auto.
seq  1  1: (   ={glob AS, glob Env, glob SS, Reader.bound, tc1, tc2, qcard, rho}
            /\ ={sk}(Card, Wrap)
            /\ Reader.cpk{1} = ReductionMod.pk{2}
            /\ Reader.cpk{1} = Wrap.pk{2}
            /\ Reader.cpk{1} = pk{2}
            /\ Env.nmap{2}.[h{2}] =
                  Some (t0, oget Env.lmap{1}.[Top.Reader], Challenge ReductionMod.unc{2})
            /\ (Reader.smap.[sidc]{1} <> None => Reader.smap{1}.[sidc{1}] = Some (t0, ReductionMod.unc{2}))
            /\ o{1} = None
            /\ t0 <= t1 <= t2 <= Env.clock{1}
            /\ t2 = t1 + (if 0%r <= tc1{1} then tc1{1} else 0%r)
            /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
            /\ resp{1} = None
            /\ ncc{1} = ReductionMod.ncc{2}
            /\ sigc{1} = ReductionMod.sigc{2}
            /\ Reader.bound{1} < 2%r * dl (oget Env.lmap{1}.[Reader]) (oget Env.lmap{1}.[Card])).
+ exists * h{2}, Env.nmap.[h]{2}; elim * => mh [|[] t0' lReader nc].
  + by exfalso=> /#.
  exists * sidc{1}, Reader.smap.[sidc]{1}; elim * => sidc sstate.
  call (:    ={glob Env, glob SS, Reader.bound}
          /\ ={sk}(Card,Wrap)
          /\ Reader.cpk{1} = ReductionMod.pk{2}
          /\ Reader.cpk{1} = Wrap.pk{2}
          /\ Env.nmap{2}.[mh] = Some (t0', lReader, nc)
          /\ (Reader.smap{1}.[sidc] <> None => Reader.smap{1}.[sidc] = sstate)
          /\ (well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock){2}
          /\ t0 <= t1 <= t2 <= Env.clock{1}); auto.
  + by conseq (: ={res})=> //; sim.
  + conseq (: ={glob Env}) (:    t2 <= Env.clock
                              /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                              ==>    t2 <= Env.clock
                                  /\ well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> //.
    + conseq (: t2 <= Env.clock ==> t2 <= Env.clock) wfe_set_clock=> //.
      by proc; auto=> /#.
    by sim.
  + by conseq (: ={res})=> //; sim.
  + conseq (: true)
           (: Reader.smap.[sidc] <> None => Reader.smap.[sidc] = sstate
              ==> Reader.smap.[sidc] <> None => Reader.smap.[sidc] = sstate)=> //.
    + proc; exists * a; elim * => - []; match Timeout 1; inline *; auto=> |> &1.
      rewrite !filterE //=; case: (Reader.smap{1}.[sidc])=> |> x.
      by case: (dt Env.clock{1} x.`1 <= Reader.bound{1}).
    by sim.
  + conseq (: ={glob Env, res}) _
           (:    well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              /\ Env.nmap.[mh] = Some (t0', lReader, nc)
              ==>    well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                  /\ Env.nmap.[mh] = Some (t0', lReader, nc))=> //.
    + proc; call (:    well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                    /\ Env.nmap.[mh] = Some (t0', lReader, nc)
                    ==>    well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                        /\ Env.nmap.[mh] = Some (t0', lReader, nc))=> //.
      conseq wfe_send (:    well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
                         /\ Env.nmap.[mh] = Some (t0', lReader, nc)
                         ==> Env.nmap.[mh] = Some (t0', lReader, nc))=> //.
      proc; auto=> |> &1 wfe_inv nmap_mh; rewrite get_set_neqE //.
      by move: wfe_inv=> @/well_formed_env [#] _ _ _ + _ - /(_ mh) []; rewrite nmap_mh //#.
    by sim.
  + conseq (: ={glob Env, res}) _
           (: well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock
              ==> well_formed_env Env.mh Env.nmap Env.rh Env.mmap Env.clock)=> //.
    + by proc; call wfe_modify.
    by sim.
  + by conseq (: ={res})=> //; sim.
  by auto=> /#.
inline Env.set_clock.
inline Oracle(Card(SS), Reader(SS)).adversary_send Env.send
       Oracle(Card(SS), Reader(SS)).honest_transition.
match TReaderFinal {1} 19; first by auto=> /#.
inline Env.recv.
match Some {1} 27.
+ auto=> @/get_as_ReaderFinal />; rewrite get_set_sameE=> /> _ _ _ _ _ _ _ _ _ _ _ _.
  by exists (Env.clock + if 0%r <= tc2 then tc2 else 0%r, oget Env.lmap.[Adv], Response ReductionMod.ncc ReductionMod.sigc){m}.
rcondt{1} 28.
+ auto=> &1 @/get_as_TReaderFinal @/get_as_Some |>; rewrite !get_set_sameE ge0_dl.
  smt(dl_sym).
match None {1} 29; first by auto=> /#.
match Some {1} 30; first by auto=> /#.
inline Reader(SS).recv_response Env.get_clock.
exists * Reader.smap.[sidc]{1}; elim * => - [|state].
+ match None {1} 34.
  + by auto=> @/get_as_TReaderFinal /> + <-.
  inline Oracle(Card(SS), Reader(SS)).sendo.
  match None {1} 38; first by auto=> /#.
  by inline *; wp; call {2} SS_vll; auto.
match Some {1} 34.
+ auto=> @/get_as_TReaderFinal /> + <- - _ _ _ _ _ _ _ _ _ _ _ _.
  by exists state.
match Response {1} 36.
+ auto=> &1 @/get_as_TReaderFinal @/get_as_Some |> _ _ _ _ _ _ _ _.
  by exists ReductionMod.ncc{m} ReductionMod.sigc{m}; rewrite get_set_sameE.
inline Reader(SS).verify Env.get_clock.
exists * (Env.clock + if 0%r <= tc2 then tc2 else 0%r){1}; elim * => t3.
case: (dt (t3 + dl (oget Env.lmap.[Adv]) (oget Env.lmap.[Reader])){1} t0
       <= Reader.bound{1}); last first.
+ rcondf {1} 42.
  + auto=> |> &1 @/get_as_TReaderFinal @/get_as_Some /= {1}<- //= _ ->.
    by rewrite ge0_dl.
  conseq (: o{1} = Some Reject)=> |>.
  inline *; auto=> |>.
  by call {2} SS_vll; auto.
rcondt {1} 42.
+ auto=> |> &1 @/get_as_TReaderFinal @/get_as_Some /= {1}<- //= _ ->.
  by rewrite ge0_dl.
inline*.
match None {1} 51; first by auto; call (: true); auto.
wp; call (: true).
auto=> @/get_as_TReaderFinal @/get_as_Some @/get_as_Response |> &1 &2.
by rewrite get_set_sameE=> //= {1}<- //= _ -> //= _ _ _ _ _ _ [].
qed.

lemma asecurity &m:
  Pr[Exp_Adv_BMSec(AS, Card(SS), Reader(SS)).main() @ &m: res]
  <=   Pr[EF_CMA(Wrap(SS), ReductionMod(AS)).main() @ &m: res]
     + Pr[EF_CMA(Wrap(SS), ReductionMod(AS)).main() @ &m: (ReductionMod.unc, ReductionMod.ncc) \in Wrap.qs].
proof. by byequiv DirectMod. qed.

lemma pr_collision &m q:
  0 <= q
  =>    Pr[EF_CMA(Wrap(SS), ReductionMod(AS)).main() @ &m:
               (ReductionMod.unc, ReductionMod.ncc) \in Wrap.qs
            /\ card Wrap.qs <= q]
     <= q%r * mu1 dnonce witness.
proof.
move=> ge0_q; byphoare=> //=.
conseq (: _ ==> ReductionMod.unc \in image fst Wrap.qs /\ card Wrap.qs <= q)=> //=.
+ by move=> ncc unc qs /> nnc_in_qs _; rewrite imageP; exists (unc, ncc).
proc; inline *.
seq 19: (card Wrap.qs <= q) 1%r (q%r * mu1 dnonce witness) _ 0%r=> //=.
+ seq  1: (ReductionMod.unc \in image fst Wrap.qs) (q%r * mu1 dnonce witness) 1%r _ 0%r=> //=.
  + rnd; skip=> /> &hr qs_le_q; rewrite (Mu_mem.mu_mem _ _ (mu1 dnonce witness)).
    + by move=> x _; exact/dnonce_funi.
    apply/ler_pmul2r.
    + by exact/dnonce_fu.
    exact/le_fromint/(lez_trans _ _ _ _ qs_le_q)/fcard_image_leq.
  by conseq (: _ ==> false)=> //= &hr ->.
by hoare; conseq (: _ ==> true)=> //= &hr ->.
qed.

end section Security_Proof.
end Modify.
